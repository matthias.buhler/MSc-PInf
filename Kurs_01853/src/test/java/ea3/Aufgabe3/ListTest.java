package ea3.Aufgabe3;

import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.*;

public abstract class ListTest {

    List list;
    final String[] data = new String[] {"A", "B", "C"};

    /**
     * Subclasses must provide an implementation object of the "List" interface.
     * @return an onject of a "List" implementation
     */
    protected abstract List createList();

    @Before
    public void setUp() {
        // setup test fixture
        list = createList();
        list.addAll(Arrays.asList(data));
    }

    @Test
    public final void testAdd() {
        list.add("X");
        assertEquals("X", list.get(3));
    }

    @Test
    public final void testGet() {
        for(int i = 0; i < data.length; i++) {
            assertEquals(data[i], list.get(i));
        }
    }

    @Test
    public final void testContains() {
        for(int i = 0; i < data.length; i++) {
            assertTrue(list.contains(data[i]));
        }

        assertFalse(list.contains("Y"));
    }

    @Test
    public final void testRemove() {
        list.remove("B");

        assertTrue(list.contains("A"));
        assertFalse(list.contains("B"));
        assertTrue( list.contains("C"));
    }
}
