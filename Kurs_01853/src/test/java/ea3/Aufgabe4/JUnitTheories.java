package ea3.Aufgabe4;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import static org.junit.Assume.*;

@RunWith(Theories.class)
public class JUnitTheories {

    @DataPoint public static Double minValue = Double.MIN_VALUE;
    @DataPoint public static Double minusFour = -4.0;
    @DataPoint public static Double zero = 0.0;
    @DataPoint public static Double aHalf = 0.5;
    @DataPoint public static Double one = 1.0;
    @DataPoint public static Double two = 2.0;
    @DataPoint public static Double maxValue = Double.MAX_VALUE;

    @Theory
    public void testFloor(Double testValue) {

        Double proove = Math.floor(testValue);

        System.out.println("testFloor: " + testValue + " -> floor: " + proove);

        assertThat(proove, lessThanOrEqualTo(testValue));
        assertThat(proove, is(closeTo(testValue, 1.0)));
    }

    @Theory
    public void testLog10(Double testValue) {
        assumeThat(testValue, is(not((Double.MAX_VALUE))));
        assumeThat(testValue, is(greaterThan(0.0)));

        Double proove = Math.log10(testValue);
        Double check = Math.pow(10, proove);

        System.out.println("testLog10: " + testValue + " -> log10: " + proove + " -> check: " + check);

        assertThat(check, is(equalTo(testValue)));
    }
}
