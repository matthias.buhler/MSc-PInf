package ea2.Aufgabe2;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author matthias
 */
public class VectorTest {

    @Test
    public void testSet() {
        System.out.println("set");
        Vector<Integer> vector = new Vector();
        vector.extend(10);

        vector.set(0, -123456789);
        vector.set(1, null);
        vector.set(9, 77);

        try {
            vector.set(10, 1);
            fail("'assert' did not catch presumption violation.");
        } catch(AssertionError aerr) {}

        assertNull(vector.get(8));
        assertThat(vector.get(9), is(77));

    }

    @Test
    public void testGet() {
        System.out.println("get");
        Vector<Integer> vector = new Vector();
        vector.extend(10);

        vector.set(9, 99);
        Integer i = vector.get(9);
        assertThat(i, is(99));

        try {
            vector.get(10);
            fail("'assert' did not catch presumption violation.");
        } catch(AssertionError aerr) {}
    }

    @Test
    public void testExtend() {
        System.out.println("extend");

        Vector<Integer> vector = new Vector();
        vector.extend(90);
        vector.extend(9);

        vector.set(0, 33);

        try {
            vector.extend(-5);
            fail("'assert' did not catch presumption violation.");
        } catch(AssertionError aerr) {}


        try {
            vector.extend(7);
            fail("'assert' did not catch presumption violation.");
        } catch(AssertionError aerr) {}

        vector.extend(1);

        try {
            vector.extend(1);
            fail("'assert' did not catch presumption violation.");
        } catch(AssertionError aerr) {}
    }

    @Test
    public void testPrune() {
        System.out.println("prune");

        Vector<Integer> vector = new Vector();
        vector.extend(55);

        vector.set(0, 33);
        vector.set(54, 11);

        try {
            vector.prune(-5);
            fail("'assert' did not catch presumption violation.");
        } catch(AssertionError aerr) {}


        try {
            vector.prune(56);
            fail("'assert' did not catch presumption violation.");
        } catch(AssertionError aerr) {
            System.out.println("  ## catched AssertionError: prune value was too big!");
        }

        vector.prune(55);

        try {
            vector.prune(2);
            fail("'assert' did not catch presumption violation.");
        } catch(AssertionError aerr) {}
    }

}
