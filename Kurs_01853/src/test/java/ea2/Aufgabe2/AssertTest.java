package ea2.Aufgabe2;

import static org.junit.Assert.*;
import org.junit.Test;

public class AssertTest {

    @Test
    public void testAssert() {
        System.out.println("'assert' aktiviert");

        try {
            assert(true == false);
            fail("Assertion has not been activated!");
        } catch(AssertionError e) {
            System.out.println("Everything is fine :-)");
        }
    }
}
