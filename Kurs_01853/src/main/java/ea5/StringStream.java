package ea5;

public class StringStream extends Stream {

    public StringStream(String source) {
        super(source);
    }

    @Override
     public String read() {
        return source;
     }

    @Override
     public void write( String content ) {
        this.source = content;
     }
}
