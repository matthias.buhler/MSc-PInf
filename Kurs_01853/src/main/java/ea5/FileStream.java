package ea5;

public class FileStream extends Stream {

    public FileStream(String source) {
        super(source);
    }

    @Override
     public String read() {
         // open file "s" and read it ...
        return "dummy";
     }

    @Override
     public void write( String content ) {
        // open file "s" and write it ...
     }
}
