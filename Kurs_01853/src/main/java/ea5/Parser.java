package ea5;

public class Parser {

    public boolean parsingSuccessful; // indicates success of parsing

    public AST parse(Stream stream) {
        assert stream != null;

        // parse according to the content of s
        String content = stream.read();

        if (content.startsWith("foo")) {
            FooStatement fooStmt = new FooStatement();
            fooStmt.parse(content);
            return fooStmt;
        } else if (content.startsWith("bar")) {
            BarStatement barStmt = new BarStatement();
            barStmt.parse(content);
            return barStmt;
        }

        return null;
    }
}
