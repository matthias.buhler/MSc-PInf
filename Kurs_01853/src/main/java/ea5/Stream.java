package ea5;

public abstract class Stream {
    String source;

    public Stream( String source) {
        this.source = source;
    }

     public abstract String read();
     public abstract void write( String content );
}
