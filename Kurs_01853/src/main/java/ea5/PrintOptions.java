package ea5;

public class PrintOptions {

    int indentSize;
    boolean bracesOnSameLine;
    boolean whitespacesInParens;
    boolean compress;

    public int getIndentSize() {
        return indentSize;
    }
    public void setIndentSize(int indentSize) {
        this.indentSize = indentSize;
    }

    public boolean isBracesOnSameLine() {
        return bracesOnSameLine;
    }
    public void setBracesOnSameLine(boolean bracesOnSameLine) {
        this.bracesOnSameLine = bracesOnSameLine;
    }

    public boolean isWhitespacesInParens() {
        return whitespacesInParens;
    }
    public void setWhitespacesInParens(boolean whitespacesInParens) {
        this.whitespacesInParens = whitespacesInParens;
    }

    public boolean isCompress() {
        return compress;
    }
    public void setCompress(boolean compress) {
        this.compress = compress;
    }
}
