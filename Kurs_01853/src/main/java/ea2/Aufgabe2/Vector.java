package ea2.Aufgabe2;

import java.util.Objects;

public class Vector<T> {
    private static final int MAXIMUM_SIZE = 100;
    private Object[] elements = new Object[0];

    public void set(int index, T element) {
        assert(index < length());   // check premise

        Object[] backup = createBackup();                   // backup for postcondition check
        elements[index] = element;

        validate(backup, index, element);                   // check postcondition
    }

    public T get(int index) {
        assert(index < length());                           // check premise

        Object[] backup = createBackup();                   // backup for postcondition check
        T element = (T) elements[index];

        assert(Objects.equals(elements[index], element));   // check postconditions
        validate(backup, -1, null);

        return element;
    }

    public void extend(int size) {
        assert(length() < MAXIMUM_SIZE);                    // check premises
        assert(size > 0);
        assert(length() + size <= MAXIMUM_SIZE);

        Object[] backup = createBackup();                   // backup for postcondition check

        Object[] newElements = new Object[elements.length + size];
        System.arraycopy(elements, 0, newElements, 0, elements.length);
        elements = newElements;

        assert (backup.length + size == elements.length);   // check postconditions
        validate(backup, -1, null);
    }

    public void prune(int size) {
        assert(size > 0);                                   // check premises
        assert(size <= length());

        Object[] backup = createBackup();                   // backup for postcondition check

        Object[] newElements = new Object[elements.length - size];
        System.arraycopy(elements, 0, newElements, 0, newElements.length);
        elements = newElements;

        assert (backup.length - size == elements.length);   // check postconditions
        validate(backup, -1, null);
    }

    public int length() {
        return elements.length;
    }


    /*
     * Create a backup copy of the vector for later assertion
     */
    private Object[] createBackup() {
        Object[] backup = new Object[elements.length];
        System.arraycopy(elements, 0, backup, 0, elements.length);
        return backup;
    }

    /*
     * Check the backup against the original vector.
     * Leave out the setter position 'exceptIndex'.
     * 'exceptIndex' of -1 means: Validate complete vector.
     */
    private void validate(Object[] backup, int exceptIndex, Object expectedValue) {
        // choose the length of the shorter array
        int len = elements.length < backup.length ? elements.length : backup.length;

        for(int i = 0; i < len; i++) {
            if(i >= 0 && i == exceptIndex) {
                assert(expectedValue == elements[i]);   // check changed value (if provided)
            } else {
                assert(Objects.equals(backup[i], elements[i]));
            }
        }
    }
}
