package ea2.Aufgabe4;

public abstract class OutputStream<T extends Data> {

  public abstract void write(T data);
  
  public static class TypeX {}
  public static class TypeY {}

  TypeY methodOrig(TypeX x) {
    TypeY result = new TypeY();

    return result;
  }
  
  Object method(Object xObj) {
    assert(xObj instanceof TypeX);
    TypeX x = (TypeX) xObj;
    
    TypeY result = new TypeY();

    /* operating code */
    
    assert(result instanceof TypeY);
    return result;
  }
  
}
