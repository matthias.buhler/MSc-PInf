package ea4;

import java.util.Objects;

/**
 * This class represents a group of filesystem elements within a container.
 */
public class Group extends AbstractFileSystemContainer {

    public Group(String name, Folder parent) {
        super(name, parent);
    }

    @Override
    public void open() {
        super.open();
        getElements().stream().forEach(elem -> elem.open());
    }

    @Override
    public void add(AbstractFileSystemElement element) {
        if(Objects.equals(element.getParentElement(), getParentElement())) {
            super.add(element);
        } else {
            System.err.println(getName() + ": cannot add '" + element.getName() + "' to this group!");
        }
    }

    @Override
    public void move(Folder target) {
        throw new UnsupportedOperationException("Group cannot be moved!");
    }
}
