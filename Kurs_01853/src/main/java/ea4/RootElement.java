package ea4;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a root element of a filesystem.
 */
public class RootElement extends Folder {

    public RootElement(String name) {
        super(name, null);
    }

    @Override
    public Folder getParentElement() {
        return null;
    }

    @Override
    public void move(Folder target) {
        throw new UnsupportedOperationException("Root cannot be moved!");
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Root cannot be deleted!");
    }

    public void format() {
        System.out.println(getName() + ": formatting");
        List<AbstractFileSystemElement> _copyList = new ArrayList<>(getElements());
        _copyList.stream().forEach(elem -> elem.delete());
    }
}
