package ea4;

/**
 * determined visitor class
 */
public class FileSystemDirectoryVisitor extends FileSystemVisitor {

    @Override
    public boolean visit(AbstractFileSystemContainer container) {
        System.out.println(container.getHierarchyIndent() + container.getName());
        return !container.getElements().isEmpty();
    }

    @Override
    public boolean visit(File file) {
        System.out.println(file.getHierarchyIndent() + file.getName());
        return false;
    }
}
