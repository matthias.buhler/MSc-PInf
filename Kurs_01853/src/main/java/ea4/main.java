package ea4;

public class main {

    public static void main(String[] args) {
//        test1();
        test2();
    }

    public static void test1() {
        System.out.println("------------- Test 1 -------------");

        RootElement root = new RootElement("root");
        FileSystem.addRoot(root);

        Folder folder1 = new Folder("folder1", root);
        Folder folder2 = new Folder("folder2", root);
        Folder folder3 = new Folder("folder3", root);

        Folder folder11 = new Folder("folder11", folder1);
        File file111 = new File("file111", folder11);
        File file11 = new File("file11", folder1);
        File file12 = new File("file12", folder1);
        File file13 = new File("file13", folder1);
        file13.open();

        folder11.move(folder3);

        Folder folder21 = new Folder("folder21", folder2);
        File file211 = new File("file211", folder21);
        File file21 = new File("file21", folder2);
        File file22 = new File("file22", folder2);
        File file23 = new File("file23", folder2);
        Group group21 = new Group("group21", folder2);
        group21.add(folder21);
        group21.add(file211);  // <-- must be rejected (other container)
        group21.add(file21);
        group21.add(file22);

        file23.delete();
        group21.add(file23);  // <-- must be rejected (already deleted)
        group21.open();

        file211.move(folder3);

        folder2.delete();

        folder3.open();

        FileSystemVisitor visitor = new FileSystemDirectoryVisitor();
        FileSystem.visitAllRoots(visitor);

        root.format();
    }

    public static void test2() {
        System.out.println("------------- Test 2 -------------");

        // new root
        FileSystem. createNewElement ( "c" );
        // new folder "d1"
        FileSystem. createNewElement ( "c|d1" );
        // new folder "d2"
        FileSystem. createNewElement ( "c|d2" );
        // new file "f1"
        FileSystem. createNewElement ( "c|d1|.f1" );
        // new group "g1"
        FileSystem. createNewElement ( "c|d1|;g1" );
        // new folder "d2" and file "f2"
        FileSystem. createNewElement ( "c|d1|d2|.f2" );
        // new folders "d3" and "d3_1" and file "f3"
        FileSystem. createNewElement ( "c|d3|d3_1|.f3" );

        FileSystemVisitor visitor = new FileSystemDirectoryVisitor();
        FileSystem.visitAllRoots(visitor);

        // format all roots
        FileSystem.getRootElements().stream().forEach(elem -> elem.format());
    }
}
