package ea4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class represents a complete filesystem with all of its elements and features.
 */
public class FileSystem {

    public static final String SEPARATOR = "\\|";
    public static final String FILE_PREFIX = ".";
    public static final String GROUP_PREFIX = ";";

    private final static List<RootElement> rootElements = new ArrayList<>();

    public static List<RootElement> getRootElements() {
        return rootElements;
    }

    public static void addRoot(RootElement rootElement) {
        System.out.println("Filesystem: adding root'" + rootElement.getName() + "'");
        rootElements.add(rootElement);
    }

    public static void removeRoot(RootElement rootElement) {
        System.out.println("Filesystem: removing root'" + rootElement.getName() + "'");
        rootElements.remove(rootElement);
    }

    public static AbstractFileSystemElement createNewElement(String path) {
        System.out.println("creating '" + path + "'");

        List<String> parts = new ArrayList<>(Arrays.asList(path.split(SEPARATOR)));

        String rootName = parts.remove(0);
        System.out.println("... root: '" + rootName + "'");

        RootElement root = getRootElement(rootName);
        if(root == null) {
            root = new RootElement(rootName);
            addRoot(root);
        }

        AbstractFileSystemContainer currentContainer = root;

        for(String elementName : parts) {
            System.out.println("... element: '" + elementName + "'");
            AbstractFileSystemElement newElem = currentContainer.getElement(elementName);

            if(newElem == null) {
                // Files and groups are 'leaf' components. So processing stopps here.
                if(elementName.startsWith(FILE_PREFIX)) {
                    return new File(elementName.substring(1), (Folder)currentContainer);
                } else if(elementName.startsWith(GROUP_PREFIX)) {
                    return new Group(elementName.substring(1), (Folder)currentContainer);
                }

                newElem = new Folder(elementName, (Folder)currentContainer);
            }

            currentContainer = (AbstractFileSystemContainer)newElem;
        }

        return currentContainer;
    }

    public static RootElement getRootElement(String name) {
        for(RootElement rootElement : rootElements) {
            if(rootElement.getName().equals(name)) {
                return rootElement;
            }
        }
        return null;
    }

    public static void visitAllRoots(FileSystemVisitor visitor) {

        for(RootElement rootElement : rootElements) {
            if(!rootElement.accept(visitor)) {
                break;
            }
        }
    }
}
