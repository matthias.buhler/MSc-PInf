package ea4;

/**
 * This class represents a file as leaf element of a file system.
 */
public class File extends AbstractFileSystemElement {

    public File(String name, Folder parent) {
        super(name, parent);
    }

    @Override
    public boolean accept(FileSystemVisitor visitor) {
        return visitor.visit(this);
    }
}
