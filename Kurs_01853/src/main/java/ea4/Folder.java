package ea4;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a folder in a filesystem.
 */
public class Folder extends AbstractFileSystemContainer {

    public Folder(String name, Folder parent) {
        super(name, parent);
    }

    @Override
    public void delete() {
        List<AbstractFileSystemElement> _copyList = new ArrayList<>(getElements());
        _copyList.stream().forEach(elem -> elem.delete());
        super.delete();
    }
}
