package ea4;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents any kind of container elements in a filesystem.
 */
public abstract class AbstractFileSystemContainer extends AbstractFileSystemElement {

    private final List<AbstractFileSystemElement> elements;

    public AbstractFileSystemContainer(String name, Folder parent) {
        super(name, parent);
        elements = new ArrayList<>();
    }

    public List<AbstractFileSystemElement> getElements() {
        return elements;
    }

    public void add(AbstractFileSystemElement element) {
        System.out.println(getName() + ": adding '" + element.getName() + "'");
        elements.add(element);
    }

    public void remove(AbstractFileSystemElement element) {
        System.out.println(getName() + ": removing '" + element.getName() + "'");
        elements.remove(element);

        // remove elements also from groups
        elements.stream()
            .filter(elem -> elem instanceof Group)
            .forEach(grp -> ((Group)grp).remove(element));
    }

    public AbstractFileSystemElement getElement(String name) {
        for(AbstractFileSystemElement element : elements) {
            if(element.getName().equals(name)) {
                return element;
            }
        }
        return null;
    }

    @Override
    public boolean accept(FileSystemVisitor visitor) {
        if(visitor.visit(this)) {
            elements.stream().forEach(elem -> elem.accept(visitor));
        }
        return true;
    }
}
