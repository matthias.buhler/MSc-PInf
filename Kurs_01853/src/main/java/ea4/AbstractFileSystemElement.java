package ea4;

/**
 * This class implements the most common members and methods of filesystem elements.
 */
public abstract class AbstractFileSystemElement {

    private Folder parent;
    private String name;

    public AbstractFileSystemElement(String name, Folder parent) {
        assert name != null;
        assert !name.isEmpty();

        this.name = name;
        this.parent = parent;

        if(parent != null) {
            parent.add(this);
        }
    }

    public Folder getParentElement() {
        return parent;
    }

    public void open() {
        System.out.println("opening '" + name + "'");
    }

    public void move(Folder target) {
        System.out.println("moving '" + name + "' to '" + target.getName() + "'");
        this.parent.remove(this);
        target.add(this);

        this.parent = target;
    }

    public void delete() {
        System.out.println("deleting '" + name + "'");
        parent.remove(this);
        parent = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("renaming '" + this.name + "' to '" + name + "'");
        this.name = name;
    }

    public String getHierarchyIndent() {
        Folder level = parent;
        StringBuilder sb = new StringBuilder();

        while (level != null) {
            sb.append(" ");
            level = level.getParentElement();
        }

        return sb.toString();
    }

    abstract public boolean accept(FileSystemVisitor visitor);
}
