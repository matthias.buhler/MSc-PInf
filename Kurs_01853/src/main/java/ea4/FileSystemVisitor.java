package ea4;

/**
 * determined visitor class
 */
public abstract class FileSystemVisitor {

    public boolean visit(AbstractFileSystemContainer container) {
        return true;
    }

    public boolean visit(File file) {
        return true;
    }

    public boolean postVisit(AbstractFileSystemContainer container) {
        return true;
    }

    public boolean postVisit(File file) {
        return true;
    }
}
