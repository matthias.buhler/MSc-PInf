package ea6.Aufgabe1;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.apache.commons.io.IOUtils;
    
public class Panzerknacker {
    
    public static void main(String[] args) {
        Panzerknacker instance = new Panzerknacker();
        ResourceClassLoader rcl = new ResourceClassLoader();
        
        // print reflection data of classes
        String[] classesToExamine = {"Safe", "Decoder", "Inscription"};
        for(String clazzName : classesToExamine) {
            Class clazz = rcl.loadStrangeClass("ea_6_1", clazzName);
            instance.analyzeClass(clazz);
        }
    }
    
    public static class ResourceClassLoader extends ClassLoader {
        public Class loadStrangeClass(String packet, String name) {
            String resourcePath = "/ea6/Aufgabe1/" + name + ".class";
            String fullClassName = packet + "." + name;
                
            try (InputStream is = Panzerknacker.class.getResourceAsStream(resourcePath);
            ) {
                byte[] bytes = IOUtils.toByteArray(is);
                return defineClass(fullClassName, bytes, 0, bytes.length);
            } catch(IOException ex) {
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }
        }
    }
    
    private void analyzeClass(Class clazz) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n-------------------------------\n")
          .append("Class: \n  ");
        
        Annotation[] annotations = clazz.getAnnotations();
        for(Annotation ann : annotations) {
            sb.append(ann.toString()).append(" ");
        }
              
        sb.append(Modifier.toString(clazz.getModifiers())).append(" ")
        .append(clazz.getName());
        
        Class superclass = clazz.getSuperclass();
        if(superclass != null) {
          sb.append(" extends ").append(clazz.getSuperclass().getName());
        }
        
        Class[] interfaces = clazz.getInterfaces();
        if(interfaces.length > 0) {
          sb.append(" implements ");
            for(Class iface : interfaces) {
              sb.append(iface.getName());
            }
        }
        
        sb.append("\nFields:");
        for(Field field : clazz.getFields()) {
            sb.append("\n  ");
            Annotation[] ann_field = field.getAnnotations();
            for(Annotation ann : ann_field) {
                sb.append(ann.toString()).append(" ");
            }
            sb.append(field.getName());
            sb.append(" (").append(field.getType().getName()).append(")");
        }

        sb.append("\nConstructors:");
        for(Constructor constructor : clazz.getConstructors()) {
            sb.append("\n  ");
            Annotation[] ann_constr = constructor.getAnnotations();
            for(Annotation ann : ann_constr) {
                sb.append(ann.toString()).append(" ");
            }
            sb.append(constructor.toGenericString());
        }
        
        sb.append("\nMethods:");
        for(Method method : clazz.getDeclaredMethods()) {
            sb.append("\n  ");
            Annotation[] ann_meth = method.getAnnotations();
            for(Annotation ann : ann_meth) {
                sb.append(ann.toString()).append(" ");
            }
            sb.append(method.toGenericString());
        }
        
        System.out.println(sb.toString());
    }
}
