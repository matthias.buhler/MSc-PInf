package ea1.Aufgabe3;

/**
 * Interface für die "Worker"-Klasse
 */
public interface IWorker {

    public void writeToConsole(IStreamReader reader);

}
