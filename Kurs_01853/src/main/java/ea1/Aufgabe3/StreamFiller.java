package ea1.Aufgabe3;

class StreamFiller {

    private final IStreamWriter streamWriter;

    /**
     * Konstruktor mit Injektion der Stream-Referenz
     */
    public StreamFiller(IStreamWriter streamWriter) {

        // Absicherung der Stream-Referenz
        if(streamWriter != null) {
            this.streamWriter = streamWriter;
        } else {
            throw new RuntimeException("Die Klasse 'StreamFiller' muss mit einem gültigen Stream initialisiert werden!");
        }
    }

    /**
     * Ausführende Methode, nicht mehr 'static', sondern als Instanz-Methode
     */
    public void fillStream() {
        streamWriter.writeString("This is a");
        streamWriter.writeString(" stream.");
    }
}
