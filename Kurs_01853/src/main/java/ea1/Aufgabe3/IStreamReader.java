package ea1.Aufgabe3;

/**
 * "StreamPrinter" nutzt das "IStreamReader"-Interface. Deshalb muss die "apply"-
 * Methode hier ergänzt werden.
 */
public interface IStreamReader {

    public char readChar();
    public void apply(IWorker worker);

}
