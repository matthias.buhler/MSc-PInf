package ea1.Aufgabe3;

class Stream implements IStreamReader, IStreamWriter {

    private String content = "";
    int index = 0;

    @Override
    public char readChar() {
        if (index < content.length()) {
            return content.charAt(index++);
        } else {
            return 0;
        }
    }

    @Override
    public void writeString(String str) {
        content += str;
    }

    /**
     * Die Methode "apply" führt die Arbeitsmethode "writeToConsole" der übergebenen
     * Arbeitsklasse aus.
     */
    @Override
    public void apply(IWorker worker) {
        worker.writeToConsole(this);
    }
}
