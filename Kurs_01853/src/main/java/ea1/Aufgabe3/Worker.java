package ea1.Aufgabe3;

/**
 * "Worker"-Klasse mit der Arbeitsmethode
 */
public class Worker implements IWorker {

    @Override
    public void writeToConsole(IStreamReader reader) {
        char ch = reader.readChar();
        while (ch != 0) {
            System.out.print(ch);
            ch = reader.readChar();
        }
    }

}
