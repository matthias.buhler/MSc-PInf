package ea1.Aufgabe3;

class StreamPrinter {

    private IStreamReader streamReader;

    /**
     * Setter-Injektion für die Stream-Referenz
     */
    public void setStreamReader(IStreamReader streamReader) {
        this.streamReader = streamReader;
    }

    /**
     * Ausführende Methode, nicht mehr 'static', sondern als Instanz-Methode
     */
    public void printStream() {

        /* Absicherung der Stream-Referenz. Dies muss hier erfolgen, weil nicht sicher ist,
         * ob der Setter wirklich aufgerufen wurde.
         */
        if(streamReader != null) {
            streamReader.apply(new Worker());
        } else {
            throw new RuntimeException("Die Klasse 'StreamPrinter' muss mit einem gültigen Stream initialisiert werden!");
        }

    }
}
