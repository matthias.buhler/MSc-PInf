package ea1.Aufgabe3;

public class StreamProcessing {

    public static void main(String[] args) {
        Stream stream = new Stream();

        // 'StreamFiller'-Erzeugung mit Konstruktor-Injektion
        StreamFiller streamFiller = new StreamFiller(stream);

        // 'StreamPrinter'-Erzeugung mit Setter-Injektion
        StreamPrinter streamPrinter = new StreamPrinter();
        streamPrinter.setStreamReader(stream);

        // parameterloser Aufruf - beliebig wiederholbar
        for(int i=0; i<10; i++) {
            streamFiller.fillStream ();
            streamPrinter.printStream ();
        }
    }
}
